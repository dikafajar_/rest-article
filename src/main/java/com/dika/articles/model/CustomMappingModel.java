package com.dika.articles.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@SqlResultSetMapping(name = "QueryNativeWithJoin", entities = {
        @EntityResult( entityClass = CustomMappingModel.class, fields = {
                @FieldResult(name = "id", column = "c.id"),
                @FieldResult(name = "name", column = "c.name"),
                @FieldResult(name = "comment", column = "c.comment"),
                @FieldResult(name = "title_article", column = "title_article"),
                @FieldResult(name = "author", column = "author"),
        })
})
public class CustomMappingModel {
    @Id
    private int id;
    private String name;
    private String comment;
    private String title_article;
    private String author;
}
