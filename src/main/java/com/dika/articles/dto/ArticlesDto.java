package com.dika.articles.dto;

import lombok.Data;

@Data
public class ArticlesDto {
    private int id;
    private String title;
    private String body;
}
