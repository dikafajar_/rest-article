package com.dika.articles.services;

import com.dika.articles.dto.ArticlesDto;
import com.dika.articles.model.Articles;
import com.dika.articles.repository.ArticlesRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ArticlesService {

    @Autowired
    private ArticlesRepository articlesRepository;

    public List<ArticlesDto> getArticlesDto(){
        List<Articles> articles = articlesRepository.findAll();
        ModelMapper mm = new ModelMapper();

        return articles.stream().map(x->mm.map(x, ArticlesDto.class)).collect(Collectors.toList());
    }

    public List<ArticlesDto> getArticleByIdDto(int id){
        Optional<Articles> articles = articlesRepository.findById(id);
        ModelMapper mm = new ModelMapper();

        return articles.stream().map(x->mm.map(x, ArticlesDto.class)).collect(Collectors.toList());
    }
}
