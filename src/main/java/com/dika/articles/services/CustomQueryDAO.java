package com.dika.articles.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Service
public class CustomQueryDAO {

    @Autowired
    private EntityManager em;

    public List getCustomQueryNative(int id){
        String nativeQueryScript = "SELECT c.id, c.name, c.comment, ar.title as title_article, au.name as author FROM comments" +
                " c INNER JOIN articles ar ON c.articles_id = ar.id INNER JOIN author" +
                " au ON au.id = ar.author_id WHERE c.id = :id";
        Query q = em.createNativeQuery(nativeQueryScript, "QueryNativeWithJoin");

        return q.setParameter("id", id).getResultList();
    }
}
