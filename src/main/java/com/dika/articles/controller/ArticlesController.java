package com.dika.articles.controller;

import com.dika.articles.dto.ArticlesDto;
import com.dika.articles.model.Articles;
import com.dika.articles.repository.ArticlesRepository;
import com.dika.articles.services.ArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/article")
public class ArticlesController {

    @Autowired
    private ArticlesRepository articlesRepository;

    @Autowired
    private ArticlesService articlesService;

    @GetMapping("/getArticles")
    public ResponseEntity getArticle() {
        List<Articles> articles = articlesRepository.findAll();
        if (articles.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok(articles);
    }

    @GetMapping("/getArticlesDto")
    public ResponseEntity getArticleDto() {
        List<ArticlesDto> articlesDtos = articlesService.getArticlesDto();
        if (articlesDtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok(articlesDtos);
    }

    @GetMapping("/getArticleByIdDto/{id}")
    public ResponseEntity getArticleDto(@PathVariable int id) {
        List<ArticlesDto> articlesDtos = articlesService.getArticleByIdDto(id);
        if (articlesDtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok(articlesDtos);
    }

    @PostMapping("/saveArticle")
    public ResponseEntity<Articles> saveArticles(@RequestBody Articles articles){
        Articles a = articlesRepository.save(articles);
        return new ResponseEntity<>(a, HttpStatus.CREATED);
    }
}
