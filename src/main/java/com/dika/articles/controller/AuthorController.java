package com.dika.articles.controller;

import com.dika.articles.model.Author;
import com.dika.articles.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "author")
public class AuthorController {

    @Autowired
    private AuthorRepository authorRepository;

    @GetMapping("/getAuthors")
    public ResponseEntity getAuthor(){
        List<Author> authors = authorRepository.findAll();
        if (authors.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok(authors);
    }

    @GetMapping("/getAuthorById/{id}")
    public ResponseEntity getAuthorById(@PathVariable int id){
        Optional<Author> authors = authorRepository.findById(id);
        if (authors.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok(authors);
    }

    @PostMapping("/saveAuthor")
    public ResponseEntity<Author> saveAuthor(@RequestBody Author author){
        Author a = authorRepository.save(author);
        return new ResponseEntity<>(a, HttpStatus.CREATED);
    }
}
