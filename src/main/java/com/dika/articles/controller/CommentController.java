package com.dika.articles.controller;

import com.dika.articles.model.Comments;
import com.dika.articles.repository.CommentRepository;
import com.dika.articles.services.CustomQueryDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(path = "/comment")
public class CommentController {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private CustomQueryDAO customQueryDAO;

    @GetMapping("/getComments")
    public ResponseEntity getComment(){
        List<Comments> comments = commentRepository.findAll();
        if (comments.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok(comments);
    }

    @PostMapping("/saveComment")
    public ResponseEntity<Comments> saveComment(@RequestBody Comments comments){
        Comments c = commentRepository.save(comments);
        return new ResponseEntity<>(c, HttpStatus.CREATED);
    }

    @GetMapping("/nativeQuery")
    public ResponseEntity getNativeQuery(@RequestParam int id){
        List customMappingModels = customQueryDAO.getCustomQueryNative(id);
        if (customMappingModels.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok(customMappingModels);
    }
}
